import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Button from "../components/Button";
import Modal from "../components/Modal";
import CartItem from "../components/CartItem";
import { setModalType } from "../store/modalSlice";

const Cart = () => {
  const [itemsInCart, setItemsInCart] = useState([]);
  const [activeProduct, setActiveProduct] = useState("");

  const dispatch = useDispatch();
  const modalType = useSelector((state) => state.modal.modalType);

  const checkActiveProduct = (product) => {
    setActiveProduct(product);
  };

  const openDeleteModal = (product) => {
    dispatch(setModalType("Delete"));
    checkActiveProduct(product);
  };

  const getProductsFromCartArray = () => {
    return JSON.parse(localStorage.getItem("productsInCart"));
  };

  const removeProductFromCart = (product) => {
    const productCartArray = getProductsFromCartArray();

    const newProductCartArray = productCartArray.filter(
      (productFromArray) => productFromArray.key !== product.key
    );

    localStorage.setItem("productsInCart", JSON.stringify(newProductCartArray));

    setItemsInCart([...newProductCartArray]);

    dispatch(setModalType("None"));
    setActiveProduct("");
  };

  const calculateTotal = () => {
    const productCartArray = getProductsFromCartArray();

    let totalSum = 0;

    productCartArray.forEach((product) => {
      totalSum = totalSum + product.quantity * product.price;
    });
    return totalSum;
  };

  useEffect(() => {
    setItemsInCart(() => [...getProductsFromCartArray()]);
  }, []);

  return (
    <>
      <ul className="cart-container">
        {itemsInCart.map((product) => (
          <CartItem
            cardData={product}
            buttons={
              <>
                <Button clickFunc={() => openDeleteModal(product)} text="X" />
              </>
            }
          />
        ))}
      </ul>
      <div className="cart-total">Total: € {calculateTotal()}</div>

      {modalType === "Delete" && (
        <Modal
          header="Delete?"
          closeButton={true}
          text="Are you sure you want to remove this product from your cart?"
          actions={
            <>
              <Button
                clickFunc={() => removeProductFromCart(activeProduct)}
                text="Yes"
              />
              <Button
                clickFunc={() => dispatch(setModalType("None"))}
                text="No"
              />
            </>
          }
        />
      )}
    </>
  );
};

export { Cart };
