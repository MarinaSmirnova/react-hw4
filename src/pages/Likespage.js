import React, { useState, useEffect } from "react";
import ProductList from "../components/ProductList";
import ProductCard from "../components/ProductCard";
import Button from "../components/Button";
import Modal from "../components/Modal";
import Star from "../components/Star";

const Likespage = () => {
  const [likedProducts, setLikedProducts] = useState([]);
  const [openedModal, setOpenModal] = useState("None");
  const [activeProduct, setActiveProduct] = useState("");

  const checkActiveProduct = (product) => {
    setActiveProduct(product);
  };

  const openModal = (modal) => {
    setOpenModal(modal);
  };

  const openConfirmModal = (product) => {
    openModal("Confirm");
    checkActiveProduct(product);
  };

  const checkStar = (product) => {
    if (checkProductInLikesArray(product)) {
      const likedProductsArray = JSON.parse(
        localStorage.getItem("likedProducts")
      );

      const newLikedProductsArray = likedProductsArray.filter(
        (productFromArray) => productFromArray.key !== product.key
      );

      localStorage.setItem(
        "likedProducts",
        JSON.stringify(newLikedProductsArray)
      );
    } else {
      addProductToArray(product, "likedProducts");
    }
  };

  const checkProductInArray = (product, array) => {
    const localArray = JSON.parse(localStorage.getItem(array));
    const findProductInArray = localArray.find((productInLokalStarage) =>
      productInLokalStarage.key.includes(product.key)
    );

    return findProductInArray ? true : false;
  };

  const checkProductInLikesArray = (product) => {
    return checkProductInArray(product, "likedProducts");
  };

  const addProductToArray = (product, array) => {
    const productArray = JSON.parse(localStorage.getItem(array));

    productArray.push(product);
    const arrayInJSON = JSON.stringify(productArray);

    localStorage.setItem(array, arrayInJSON);

    setOpenModal("None");
    setActiveProduct("");
  };

  const addProductToCart = (product) => {

    const localCartArray = JSON.parse(localStorage.getItem("productsInCart"));

    if (checkProductInArray(product, "productsInCart")) {

      const index = localCartArray.findIndex(function(productInCartArray) {
        return productInCartArray.key === product.key;
      });

      localCartArray[index].quantity ++;
    } else {
      product.quantity = 1;

      localCartArray.push(product);
      
    }

    localStorage.setItem("productsInCart", JSON.stringify(localCartArray));

    setOpenModal("None");
    setActiveProduct("");
  };

  useEffect(() => {
    const likedProducts = JSON.parse(
      localStorage.getItem("likedProducts")
    );
    setLikedProducts(() => [...likedProducts]);
  }, [checkStar]);

        
        
    


  return (
    <>
      <ProductList
        productCards={
          <>
            {likedProducts.map((product) => (
              <ProductCard
                cardData={product}
                buttons={
                  <>
                    <Star
                      clickFunc={() => checkStar(product)}
                      type={
                        checkProductInLikesArray(product) ? "Painted" : "Empty"
                      }
                    />
                    <Button
                      clickFunc={() => openConfirmModal(product)}
                      text="Add to cart"
                    />
                  </>
                }
              />
            ))}
          </>
        } 
      />
      {openedModal === "Confirm" && (
        <Modal
          modalSetter={setOpenModal}
          header="Add to cart"
          closeButton={true}
          text="Would you like to add this item to your cart?"
          actions={
            <>
              <Button
                clickFunc={() =>
                  addProductToCart(activeProduct)
                }
                text="Yes"
              />
              <Button clickFunc={() => setOpenModal("None")} text="No" />
            </>
          }
        />
      )}
    </>
  );
};

export { Likespage };
