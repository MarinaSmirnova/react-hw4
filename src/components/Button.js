import React from "react";
import PropTypes from "prop-types";

function Button({ backgroundColor, text, clickFunc }) {
  const styleProps = {
    backgroundColor: backgroundColor,
  };

  return (
    <button className="button" onClick={clickFunc} style={styleProps}>
      {text}
    </button>
  );
}

Button.propTypes = {
  text: PropTypes.string.isRequired,
  backgroundColor: PropTypes.string,
  clickFunc: PropTypes.func.isRequired,
};

Button.defaultProps = {
  backgroundColor: undefined
};

export default Button;
