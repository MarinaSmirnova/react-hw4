import React, { useState, useEffect } from "react";
import { NavLink, Outlet } from "react-router-dom";
import Header from "./Header";
import { IoStarSharp } from "react-icons/io5";
import { IoIosCart } from "react-icons/io";
import IconWithInfo from "./IconWithInfo";

const Layout = () => {
  const calculateCartItems = () => {
    const localCartArray = JSON.parse(localStorage.getItem("productsInCart"));
    const itemsSum = localCartArray.reduce((acc, product) => acc + product.quantity, 0);
    return itemsSum;
  }

  const [likesLendth, setLikesLendth] = useState(
    JSON.parse(localStorage.getItem("likedProducts")).length || 0
  );
  const [cartLendth, setCartLendth] = useState(
    calculateCartItems() || 0
  );

  const renewLikesLendth = () => {
    setLikesLendth(JSON.parse(localStorage.getItem("likedProducts")).length);
  };

  const renewCartLendth = () => {
    setCartLendth(calculateCartItems());
  };

  const updateState = (e) => {
    if (e.target.closest(".star")) {
        renewLikesLendth();
    } else if (e.target.closest(".modal__button-container .button")) {
        renewCartLendth();
    }
  };

  window.addEventListener("click", updateState);


  return (
    <>
      <Header
        elements={
          <>
            <NavLink to="/" className="header__nav-link">
              Home
            </NavLink>
            <NavLink to="/likes" className="header__nav-link">
              Likes
            </NavLink>
            <IconWithInfo icon={<IoStarSharp />} info={likesLendth} />
            <NavLink to="/cart" className="header__nav-link">
              Cart
            </NavLink>
            <IconWithInfo
              icon={<IoIosCart />}
              info={cartLendth}
            />
          </>
        }
      />
      <Outlet />
    </>
  );
};

export { Layout };
